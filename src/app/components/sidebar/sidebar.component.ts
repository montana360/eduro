import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalConfig, NgbModal, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';


declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Dashboard',  icon: 'ni-tv-2 text-primary', class: '' },
    {path: '/users', title: 'Users', icon:'fas fa-users text-yellow', class: ''},
    {path: '/competitions', title: 'Competitions', icon:'ni ni-trophy text-purple', class: ''},
    {path: '/competitors', title: 'Competitors', icon:'ni ni-paper-diploma text-blue', class: ''},
    {path: '/judges', title: 'Judges', icon:'ni ni-sound-wave text-red', class: ''},
    {path: '/posts', title: 'Posts', icon:'ni  ni-tv-2 text-yellow', class: ''},
    {path: '/profile', title: 'Profile', icon: 'ni-single-02 text-yellow', class: ''}
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
user:any;
profile:any;
token:any;
  public menuItems: any[];
  public isCollapsed = true;

  constructor(
    private router: Router,
    private modalService: NgbModal,
    
    ) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem("user"));
    this.profile = JSON.parse(localStorage.getItem("user_profile"));
    this.token = localStorage.getItem('token');
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
   });
  }
  openContent(logoutModal) {
    this.modalService.open(logoutModal, { scrollable: true, size: 'sm' });
  }
  onLogout() {
    localStorage.clear();
    this.modalService.dismissAll();
    this.router.navigate(['login']);

  }
  no() {
    this.modalService.dismissAll();
  }
}
