import { Component, OnInit, ElementRef } from '@angular/core';
import { ROUTES } from '../sidebar/sidebar.component';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { Router } from '@angular/router';
import { NgbModalConfig, NgbModal, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  user: any;
  profile: any;
  token: any;
  public focus;
  public listTitles: any[];
  public location: Location;
  constructor(location: Location, 
    private modalService: NgbModal,
    private element: ElementRef, private router: Router,) {
    this.location = location;
  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem("user"));
    this.profile = JSON.parse(localStorage.getItem("user_profile"));
    this.token = localStorage.getItem('token');
    console.log(this.user);
    console.log(this.profile);
    this.listTitles = ROUTES.filter(listTitle => listTitle);
  }
  getTitle(){
    var titlee = this.location.prepareExternalUrl(this.location.path());
    if(titlee.charAt(0) === '#'){
        titlee = titlee.slice( 1 );
    }

    for(var item = 0; item < this.listTitles.length; item++){
        if(this.listTitles[item].path === titlee){
            return this.listTitles[item].title;
        }
    }
    return 'Dashboard';
  }
  openContent(logoutModal) {
    this.modalService.open(logoutModal, { scrollable: true, size: 'sm' });
  }
  onLogout() {
    localStorage.clear();
    this.modalService.dismissAll();
    this.router.navigate(['login']);

  }
  no() {
    this.modalService.dismissAll();
  }
}
