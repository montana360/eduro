import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { IconsComponent } from '../../pages/icons/icons.component';
// import { MapsComponent } from '../../pages/maps/maps.component';
import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';
import { TablesComponent } from '../../pages/tables/tables.component';
import {JudgesComponent } from '../../pages/judges/judges.component';
import { CompetitionsComponent } from '../../pages/competitions/competitions.component';
import { UsersComponent } from '../../pages/users/users.component';
import { CompetitorsComponent } from '../../pages/competitors/competitors.component';
import { ProfileComponent } from '../../pages/profile/profile.component';
import { PostsComponent } from '../../pages/posts/posts.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent },
    { path: 'user-profile',   component: UserProfileComponent },
    { path: 'tables',         component: TablesComponent },
    { path: 'icons',          component: IconsComponent },
    { path: 'judges',         component: JudgesComponent },
    { path: 'competitions',   component: CompetitionsComponent },
    { path: 'competitors',    component: CompetitorsComponent },
    { path: 'users',          component: UsersComponent },
    { path: 'profile',        component: ProfileComponent},
    { path: 'posts',          component: PostsComponent}
];
