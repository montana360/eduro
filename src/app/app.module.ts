import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { NgxSpinnerModule } from 'ngx-spinner';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';
import { CompetitionsComponent } from './pages/competitions/competitions.component';
import { UsersComponent } from './pages/users/users.component';
import { JudgesComponent } from './pages/judges/judges.component';
import { CompetitorsComponent } from './pages/competitors/competitors.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { ToastrModule } from 'ngx-toastr';
import { PostsComponent } from './pages/posts/posts.component';
// import { SignUpComponent } from './pages/sign-up/sign-up.component';



@NgModule({
  imports: [
    FormsModule,
    HttpClientModule,
    ComponentsModule,
    NgbModule,
    RouterModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
    NgxSpinnerModule,
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    CompetitionsComponent,
    UsersComponent,
    JudgesComponent,
    CompetitorsComponent,
    ProfileComponent,
    PostsComponent,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
