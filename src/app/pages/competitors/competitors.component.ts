import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { AlertService } from "../../services/alert.service";
import { NgxSpinnerService } from "ngx-spinner";
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-competitors',
  templateUrl: './competitors.component.html',
  styleUrls: ['./competitors.component.css']
})
export class CompetitorsComponent implements OnInit {
  competitors: any;
  allUsers: any;
  competition: any;

  constructor(
    config: NgbModalConfig,
    private modalService: NgbModal,
    public auth: AuthService,
    private alert: AlertService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.getCompetition()
  }
  getCompetition() {
    this.spinner.show();
    this.auth.get("competitions").subscribe(
      response => {
        this.spinner.hide();
        if (response["data"] !== null || response["data"] !== undefined) {
          this.competition = response["data"];
          console.log(this.competition);
        } else {
          this.alert.info("No Competition Available.");
        }
      },
      error => {
        this.spinner.hide();
        this.alert.error("Error loading Competition Data");
      }
    );
  }

  getCompetitors() {
    this.spinner.show();
    this.auth.get('admin_users').subscribe(
      response => {
        // console.log(response);
        this.competitors = response['data']['data'];
        console.log(this.competitors);
        if (response['data']['data'].length > 0) {
          this.spinner.hide();
        } else {
          this.spinner.hide();
          this.alert.info('No Data available yet');
        }
      },
      error => {
        this.spinner.hide();
        this.alert.error(error['message']);
      }
    );
  }

  delete(id) {
    this.spinner.show();
    const data = {
      id: id
    };
    this.auth.destroy("delete_user", localStorage.getItem('userID'), data).subscribe(
      response => {
        this.spinner.hide();
        this.alert.success(response["message"]);
        this.getCompetitors();
      },
      error => {
        console.log(error);
        this.spinner.hide();
        this.alert.error("Error deleting data, please try again.");
      }
    );
  }

}
