import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { AlertService } from "../../services/alert.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { NgxSpinnerService } from "ngx-spinner";
import { AuthService } from '../../services/auth.service';


@Component({
  selector: 'app-competitions',
  templateUrl: './competitions.component.html',
  styleUrls: ['./competitions.component.css']
})
export class CompetitionsComponent implements OnInit {
  // formgroup
  competitionForm: FormGroup;
  competitionEditForm: FormGroup;
  judgesForm: FormGroup;
  prizeForm: FormGroup;
  // image: any;
  viewcompetition:any;
  competition: any;
  editCompetition: any;
  alldash: any;
  image_url: string = "/assets/img/default-avatar.png";
  selectedImage: any;
  // fileToUpload: File = null;
  // details for creating competition
  competitionDetails = {
    image_url: null,
    category: "",
    title: "",
    description: "",
    service_fees: ""
  };

  // details for editing competition
  competitionEditDetails = {
    
    category: "",
    title: "",
    id: "",
    description: "",
    phase: "",
    current_stage: ""
  };
  judgesDetails = {
    user_id: "",
    competition_id: "",
  };

  prizeDetails = {
    title:"",
    description:"",
    cash_price:"",
    competition_id:"",
    position: null,
  };
  user: any;
  judges: any;

  constructor(
    config: NgbModalConfig,
    private modalService: NgbModal,
    public auth: AuthService,
    private alert: AlertService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem("user"));

    // getting all Competitions
    this.getCompetition();
    this.getDash();
    this.getUsers();

    // Initializing competition form
    this.competitionForm = this.formBuilder.group({
      image_url: [null],
      category: [null],
      title: [null],
      service_fees: [null],
      description: [null]
    });

    this.judgesForm = this.formBuilder.group({
      user_id: [null],
      // competition_id:[null],
    });
    this.prizeForm = this.formBuilder.group({
      // competition_id: [null],
      title:[null],
      description:[null],
      cash_price:[null],
      position:[null]
    })

    // competition edit form
    this.competitionEditForm = this.formBuilder.group({
      category: [""],
      title: [""],
      description: [""],
      phase: [""],
      current_stage: [""]
    });
  }

  getUsers() {
    this.auth.get("admin_users").subscribe(
      response => {
        if (response["data"]['data'] !== null || response["data"]['data'] !== undefined) {
          const users = response["data"]['data'];
          this.judges = users.filter(user => user.user_type !== 'CP');
          // console.log(this.judges);
        } else {
          this.alert.info("No Competition Available.");
        }
      },
      error => {
        // this.spinner.hide();
        this.alert.error("Error loading Competition Data");
        console.log(error);
      }
    );
  }


  // Function to get all competition from server
  getCompetition() {
    this.spinner.show();
    this.auth.get("competitions").subscribe(
      response => {
        console.log(response);
        // console.log(response["data"]);
        this.spinner.hide();
        if (response["data"] !== null || response["data"] !== undefined) {
          this.competition = response["data"];
        } else {
          this.alert.info("No Competition Available.");
        }
      },
      error => {
        this.spinner.hide();
        this.alert.error("Error loading Competition Data");
        // console.log(error);
      }
    );
  }

  // Getting data of a competition
  edit(id) {
    this.spinner.show();
    this.auth.show("competition", id).subscribe(
      response => {
        console.log(response["data"]);
        this.editCompetition = response["data"][0];
        this.spinner.hide();
        this.prepareEditForm();
      },
      error => {
        this.spinner.hide();
        // console.log(error);
        this.alert.error("Could not update info, please try again later");
      }
    );
  }
  
    v(id) {
      this.auth.show('competition',id).subscribe(
        (response) => {
          console.log(response);
          this.viewcompetition = response['data'];
          console.log(this.viewcompetition);
        },
        (error) => {
          console.log(error);
          this.alert.error('Getting data unsuccessful. Please try again');
        }
      );
    }
  // Delete competition
  delete(id) {
    this.spinner.show();
    const data = {
      id: id
    };
    this.auth.destroy("remove_competition", localStorage.getItem('userID'), data).subscribe(
      response => {
        this.spinner.hide();
        this.alert.success(response["message"]);
        this.getCompetition();
      },
      error => {
        console.log(error);
        this.spinner.hide();
        this.alert.error("Error deleting data, please try again.");
      }
    );
  }

  prepareEditForm() {
    this.competitionEditForm.get("category").setValue(this.editCompetition.category);
    this.competitionEditForm.get("title").setValue(this.editCompetition.title);
    this.competitionEditForm.get("description").setValue(this.editCompetition.description);
    this.competitionEditForm.get("phase").setValue(this.editCompetition.phase);

    this.competitionEditForm.get("current_stage").setValue(this.editCompetition.current_stage);
  }


  setJudgeData() {
    // this.judgesDetails.competition_id = this.judgesForm.controls["competition_id"].value;
    this.judgesDetails.user_id = this.judgesForm.controls['user_id'].value;
  }


  setPrizeData() {
    // this.prizeDetails.competition_id = this.prizeForm.controls["competition_id"].value;
    this.prizeDetails.title = this.prizeForm.controls["title"].value;
    this.prizeDetails.description = this.prizeForm.controls["description"].value;
    this.prizeDetails.position = parseInt(this.prizeForm.controls["position"].value);
    this.prizeDetails.cash_price = this.prizeForm.controls["cash_price"].value;
  }
  // New Specimen form data
  setData() {
    // this.competitionDetails.image_url = this.competitionForm.controls[
    //   "image_url"
    // ].value;
    this.competitionDetails.category = this.competitionForm.controls[
      "category"
    ].value;
    this.competitionDetails.title = this.competitionForm.controls[
      "title"
    ].value;
    this.competitionDetails.description = this.competitionForm.controls[
      "description"
    ].value;

    this.competitionDetails.service_fees = this.competitionForm.controls[
      "service_fees"
    ].value;
  }

  setEditData() {
    // this.competitionEditDetails.competition_id=this.competition.id;
    this.competitionEditDetails.category = this.competitionEditForm.controls[
      "category"
    ].value;
    this.competitionEditDetails.title = this.competitionEditForm.controls[
      "title"
    ].value;
    this.competitionEditDetails.description = this.competitionEditForm.controls[
      "description"
    ].value;
    this.competitionEditDetails.phase = this.competitionEditForm.controls[
      "phase"
    ].value;

    this.competitionEditDetails.current_stage = this.competitionEditForm.controls[
      "current_stage"
    ].value;

  }


  addJudge() {
    this.spinner.show();
    this.setJudgeData();
    console.log(this.judgesDetails);
    this.auth
    .update("add_judge", localStorage.getItem('userID'), this.judgesDetails)
    .subscribe(
      response => {
        console.log(response);
        this.spinner.hide();
        if (response !== null || response !== undefined) {
          this.alert.success('Judge successfully added to this competition');
          this.getCompetition();
          this.getDash();
        }
      },
      error => {
        // console.log(error);
        this.spinner.hide();
        if (error.status === 500) {
          this.spinner.hide();
          this.alert.warning("Internal Server Error");
        } else {
          this.spinner.hide();
          this.alert.error("Can`t add a judge Try again later");
        }
      }
    );
  }
  addAward() {
    this.spinner.show();
    this.setPrizeData();
    console.log(this.prizeDetails);
    this.auth
    .update("add_award", localStorage.getItem('userID'), this.prizeDetails)
    .subscribe(
      response => {
        console.log(response);
        this.spinner.hide();
        if (response !== null || response !== undefined) {
          this.alert.success('Award successfully added to this competition');
          this.getCompetition();
          this.getDash();
        }
      },
      error => {
        // console.log(error);
        this.spinner.hide();
        if (error.status === 500) {
          this.spinner.hide();
          this.alert.warning("Internal Server Error");
        } else {
          this.spinner.hide();
          this.alert.error("Can`t add a judge Try again later");
        }
      }
    );
  }

  createCompetition() {
    this.spinner.show();
    this.setData();
    console.log(this.competitionDetails);
    this.spinner.hide();
    this.auth.store("add_competition", this.competitionDetails).subscribe(
      response => {
        console.log(response);
        if (response !== null || response !== undefined) {
          this.spinner.hide();
          this.alert.success('Competition added successfully');
          this.getCompetition();
        }
      },
      error => {
        // console.log(error);
        this.spinner.hide();
        if (error.status === 500) {
          this.alert.warning("Internal Server Error");
        } else {
          this.alert.error("Can`t create a request now. Try again later");
        }
      }
    );
  }

  // Function for editting a competition
  editCompet() {
    this.spinner.show();
    this.setEditData();
    console.log(this.competitionEditDetails);
    this.auth
      .update("update_competition", this.competitionEditDetails.id, this.competitionEditDetails)
      .subscribe(
        response => {
          console.log(response);
          this.spinner.hide();
          if (response !== null || response !== undefined) {
            this.alert.success(response["message"]);
            this.getCompetition();
          }
        },
        error => {
          // console.log(error);
          this.spinner.hide();
          if (error.status === 500) {
            this.spinner.hide();
            this.alert.warning("Internal Server Error");
          } else {
            this.spinner.hide();
            this.alert.error("Can`t create a request now. Try again later");
          }
        }
      );
  }

  openScrollableContent(longContent) {
    this.modalService.open(longContent, { scrollable: true, size: 'lg' });
  }
  openEditContent(editModal) {
    this.modalService.open(editModal, { scrollable: true, size: 'lg' });
  }
  openPrizeContent(prize) {
    this.modalService.open(prize, { scrollable: true, size: 'lg' });
  }

  openJudgeContent(judgesCome) {
    this.modalService.open(judgesCome, { scrollable: true, size: 'lg' });
  }
  openViewContent(view) {
    this.modalService.open(view, { scrollable: true, size: 'lg' });
  }

  getDash() {
    this.spinner.show();
    this.auth.get("get_dashboard_count").subscribe(
      response => {
        this.spinner.hide();
        this.alldash = response;
      },
      error => {
        this.spinner.hide();
        this.alert.error("Error loading Competition Data");
        // console.log(error);
      }
    );
  }


  // creating competition
  uploadLogo() {
    this.spinner.show();

    const formData = new FormData();

    formData.append('image_url', this.competitionForm.get('image_url').value);
    formData.append('category', this.competitionForm.get('category').value);
    formData.append('title', this.competitionForm.get('title').value);
    formData.append('description', this.competitionForm.get('description').value);
    formData.append('service_fees', this.competitionForm.get('service_fees').value);

    // this.setData();
    console.log(formData);

    const uploadData = new FormData();
    this.auth.update("add_competition", localStorage.getItem('userID'), formData).subscribe(
      response => {
        console.log(response);
        this.spinner.hide();
        if (response['success'] === false) {
          this.alert.warning(response['message']);
        } else {
          this.alert.success(response['message']);
          this.getCompetition();
          this.getDash();
        }
      },
      error => {
        console.log(error);
      }
    );
  }


  handleFileInput(event) {
    const file = event.target.files[0];
    this.competitionForm.get('image_url').setValue(file);
  }


  getCompetitionID(id) {
    this.judgesDetails.competition_id = id;
  }

  getCompetitionIDPrice(id) {
    this.prizeDetails.competition_id = id;
  }

  getCompetitionI(id) {
    this.competitionEditDetails.id = id;
    console.log(id);
  }
}
