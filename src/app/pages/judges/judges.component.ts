import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { AlertService } from "../../services/alert.service";
import { NgxSpinnerService } from "ngx-spinner";
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-judges',
  templateUrl: './judges.component.html',
  styleUrls: ['./judges.component.css']
})
export class JudgesComponent implements OnInit {
  judges: any;
  allJudges: any;
  judgeslength: any;
  competition: any;

  constructor(
    config: NgbModalConfig,
    private modalService: NgbModal,
    public auth: AuthService,
    private alert: AlertService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.getCompetition();
  }
  getCompetition() {
    this.spinner.show();
    this.auth.get("competitions").subscribe(
      response => {
        this.spinner.hide();
        if (response["data"] !== null || response["data"] !== undefined) {
          this.competition = response["data"];
        } else {
          this.alert.info("No Competition Available.");
        }
      },
      error => {
        this.spinner.hide();
        this.alert.error("Error loading Competition Data");
      }
    );
  }


  remove(id) {
    this.spinner.show();
    const data = {
      id: id
    };
    this.auth.update("remove_judge", localStorage.getItem('userID'), data).subscribe(
      response => {
        this.spinner.hide();
        this.alert.success(response["message"]);
        this.getCompetition();
      },
      error => {
        console.log(error);
        this.spinner.hide();
        this.alert.error("Error deleting data, please try again.");
      }
    );
  }


}
