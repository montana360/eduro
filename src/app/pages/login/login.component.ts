import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Login } from '../../interfaces/login';
import { AuthService } from '../../services/auth.service';
import { AlertService } from '../../services/alert.service';
// import { userInfo } from 'os';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  status: any;
  isLoading = false;
  user: any;
  loginForm: FormGroup;
  credentials: Login;
  token: any;
  sentCode: boolean;


  // Login Data
  loginData = {
    email: '',
    password: '',
    client_id: '2',
    client_secret: 'O4Mz7TE1SEKmw7s2GTvp3s5zQL5Y1jYaJSSEfevn',
    scope: '*'
  };
  constructor(
    private formBuilder: FormBuilder,
    private alert: AlertService,
    private auth: AuthService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {
    {
      this.loginForm = formBuilder.group({
        email: [
          null,
          Validators.compose([Validators.required, Validators.email])
        ],
        password: [
          null,
          Validators.compose([Validators.required, Validators.minLength(5)])
        ]
      });
    }

  }

  ngOnInit() {
  }
  // this is the final data required for authentication
  setCredentials() {
    (this.loginData.email = this.loginForm.controls['email'].value),
      (this.loginData.password = this.loginForm.controls['password'].value);
  }
  // function for logging in
  signIn() {
    this.isLoading = true;
    this.setCredentials();
    this.auth.authenticate(this.loginData).subscribe(
      response => {
        if (response !== null || response !== undefined) {
          localStorage.setItem("token", response["token"]['token']);
          this.checkAccessLevel(this.loginData);
          
        } 
      },
      error => {
        if (error.status === 500) {
          this.isLoading = false;
          this.alert.info("Oops something went wrong, please try again later");
        } else if (error.status === 0) {
          this.isLoading = false;
          this.alert.warning("Network Error. Please check your connectivity.");
        } else if (error.status === 401) {
          this.isLoading = false;
          this.alert.warning(
            "Username or Password Incorrect. Please check and enter again."
          );
        } else {
          this.isLoading = false;
          this.alert.info("Wrong credentials");
          // console.log(error);
        }
      }
    );
  }

  checkAccessLevel(data) {
    this.auth.store('login', data).subscribe(
      response => {
        console.log(response);
        const login = response['data']['user_type'];
        if (login === 'ADMIN') {
          localStorage.setItem('userID', response['data']['id']);
          localStorage.setItem('username', response['data']['username']);
          localStorage.setItem('user', JSON.stringify(response['data']));
          localStorage.setItem('user_profile', JSON.stringify(response['data']['profile']));
          localStorage.setItem('follows', JSON.stringify(response['data']['follows']));
          this.isLoading = false;
          this.router.navigate(['/dashboard/']);
          this.alert.success('Welcome ' + localStorage.getItem('username'));
        } else {
          localStorage.clear();
            this.isLoading = false;
            this.alert.info('ADMIN Only');
            console.log(this.alert);
            this.router.navigate(['/login/']);
        }
      },
      error => {
        console.log(error);
        this.isLoading = false;
        this.alert.error('Signing In unsuccessful, please try again later.');
      }
    );
  }
  
}
