import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { AlertService } from "../../services/alert.service";
import { NgxSpinnerService } from "ngx-spinner";
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  competitors: any;
  competition: any;
  allUsers: any;
  judges: any;
  alldash: any;
  judgeslength: any;
  // isLoading = false


  constructor(
    config: NgbModalConfig, 
    private modalService: NgbModal,
    public auth: AuthService,
    private alert: AlertService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.getUsers()
    this.getCompetition()
    this.getDash()
    this.getCompetitors()
    this.getJudges()
  }
  getUsers() {
    this.spinner.show();
    this.auth.get('admin_users').subscribe(
      response => {
        console.log(response);
        if (response['data']['data'].length > 0 ) {
          this.allUsers = response['data']['data'];
          console.log(this.allUsers);
          this.spinner.hide();
        } else {
          this.spinner.hide();
          this.alert.info('No Data available yet');
        }
      },
      error => {
        this.spinner.hide();
        this.alert.error(error['message']);
      }
    );
  }
  getCompetitors() {
    this.spinner.show();
    this.auth.get('admin_users').subscribe(
      response => {
        if (response['data']['data'].length > 0) {
          this.competitors = response['data']['data'];
          this.spinner.hide();
        } else {
          this.spinner.hide();
          this.alert.info('No Data available yet');
        }
      },
      error => {
        this.spinner.hide();
        this.alert.error(error['message']);
      }
    );
  }
  
  getCompetition() {
    this.spinner.show();
    this.auth.get("competitions").subscribe(
      response => {
        console.log(response);
        // console.log(response["data"]);
        
        if (response["data"] !== null || response["data"] !== undefined) {
          this.competition = response["data"];
          this.spinner.hide();
        } else {
          this.spinner.hide();
          this.alert.info("No Competition Available.");
        }
      },
      error => {
        this.spinner.hide();
        this.alert.error("Error loading Competition Data");
        // console.log(error);
      }
    );
  }
  getJudges() {
    this.spinner.show();
    this.auth.get('admin_users').subscribe(
      response => {
        // console.log(response);
        this.judges = response['data']['data'];
        console.log(this.judges);
        if (response['data']['data'].length > 0 )  {
          this.judges = response['data']['data'];
          this.judgeslength = response['data']['data'].length;
          console.log(this.judgeslength.user_type == "JD");
          this.spinner.hide();
        } else {
          this.spinner.hide();
          this.alert.info('No Data available yet');
        }
      },
      error => {
        this.spinner.hide();
        this.alert.error(error['message']);
      }
    );
  }

  getDash() {
    this.spinner.show();
    this.auth.get("get_dashboard_count").subscribe(
      response => {
        console.log(response);
        // console.log(response["data"]);
        this.spinner.hide();
          this.alldash = response;
          console.log(this.alldash.users);
      },
      error => {
        this.spinner.hide();
        this.alert.error("Error loading Competition Data");
        // console.log(error);
      }
    );
  }

  

}
