import { Component, OnInit } from '@angular/core';
import { AlertService } from "../../services/alert.service";
import { AuthService } from "../../services/auth.service";

import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  id: string;
  username: string;

  profileForm: FormGroup;
  profileEditForm: FormGroup;
  url: any;
  all_users: any;
  user: any;
  editProfile: any;
  profile: any;

  profileEditDetails = {
    username: "",
    fullname: "",
    title: "",
    phone: "",
    personal_message:"",

  };


  constructor(
    private auth: AuthService,
    private alert: AlertService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem("user"));
    this.profile = JSON.parse(localStorage.getItem("user_profile"));
    // this.username = localStorage.getItem("fullname");
    console.log(this.profile.profile_image);
    // this.getUser();

    // initializing edit form for user
    this.profileEditForm = this.formBuilder.group({
      username: ["", Validators.required],
      title: ["", Validators.required],
      fullname: ["", Validators.required],
      // address: ["",Validators.required],
      phone: ["", Validators.required],
      personal_message: ["", Validators.required],
      user_type: ["", Validators.required],
    });
    this.prepareEditForm();
  }

  // Get user 
  getUser() {
    this.spinner.show();
    this.auth.show('admin_users', this.user.id).subscribe(
      response => {
        // this.user = response['user'];
        this.prepareEditForm();
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
        this.alert.error('Error loading user Data');
        // console.log(error);
      }
    );
  }
  //  setting values for edit form
  prepareEditForm() {
    this.profileEditForm.get("username").setValue(this.user.username);
    this.profileEditForm.get("title").setValue(this.user.title);
    this.profileEditForm.get("fullname").setValue(this.user.fullname);
    // this.profileEditForm.get("address").setValue(this.user.address);
    this.profileEditForm.get("phone").setValue(this.user.phone);
    this.profileEditForm.get("personal_message").setValue(this.user.personal_message);
    this.profileEditForm.get("user_type").setValue(this.user.user_type);
    
  }
  setEditData() {
    
    this.profileEditDetails.username = this.profileEditForm.controls[
      "username"
    ].value;
    this.profileEditDetails.title = this.profileEditForm.controls[
      "title"
    ].value;
    this.profileEditDetails.fullname = this.profileEditForm.controls["fullname"].value;
    this.profileEditDetails.phone = this.profileEditForm.controls["phone"].value;
    this.profileEditDetails.personal_message = this.profileEditForm.controls["personal_message"].value;
  }
  
  editUser() {
    this.spinner.show();
    this.setEditData();
    // console.log(this.profileEditDetails);
    this.auth
      .update("update_user", localStorage.getItem('userID'), this.profileEditDetails)
      .subscribe(
        response => {
          this.spinner.hide();
          if (response !== null || response !== undefined) {
            console.log(response)
            this.alert.success(response["message"]);
            this.user = JSON.parse(localStorage.getItem("user"));

          }
        },
        error => {
          console.log(error);
          this.spinner.hide();
          if (error.status === 500) {
            this.alert.warning(
              "Can not update the information, please check the data"
            );
          } else {
            this.alert.error("User update failed, try again later.");
          }
        }
      );
  }



  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        this.url = event.target.result;
      }
    }
  }
}
