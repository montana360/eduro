import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { AlertService } from "../../services/alert.service";
import { NgxSpinnerService } from "ngx-spinner";
import { AuthService } from '../../services/auth.service';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  makeJudgeForm: FormGroup;
  makeCompetitorForm: FormGroup;

  allUsers: any;
  competition: any;
  userslength: any;
  // details for making Judge of a competiton
  judgeDetails = {
    competition_name: ""
  }

  // details for adding a User to a competiton
  competitiorDetails = {
    competition_name: ""
  }



  constructor(
    private formBuilder: FormBuilder,
    config: NgbModalConfig, 
    private modalService: NgbModal,
    public auth: AuthService,
    private alert: AlertService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    // getting all Competitions
    this.getUsers();
    this.getCompetition();

    this.makeJudgeForm = this.formBuilder.group({
      competiton_name: [null]

    })
    this.makeCompetitorForm = this.formBuilder.group({
      competiton_name: [null]
    })
  }
  getCompetition() {
    this.spinner.show();
    this.auth.get("competitions").subscribe(
      response => {
        // console.log(response);
        // console.log(response["data"]);
        this.spinner.hide();
        if (response["data"] !== null || response["data"] !== undefined) {
          this.competition = response["data"];
        } else {
          this.alert.info("No Competition Available.");
        }
      },
      error => {
        this.spinner.hide();
        this.alert.error("Error loading Competition Data");
        // console.log(error);
      }
    );
  }
  getUsers() {
    this.spinner.show();
    this.auth.get('admin_users').subscribe(
      response => {
        // console.log(response);
        if (response['data']['data'].length > 0 ) {
          this.allUsers = response['data']['data'];
          this.userslength = response['data']['data'].length;
          // console.log(this.allUsers);
          this.spinner.hide();
        } else {
          this.spinner.hide();
          this.alert.info('No Data available yet');
        }
      },
      error => {
        this.spinner.hide();
        this.alert.error(error['message']);
      }
    );
  }

  delete(id) {
    // console.log(id);
    this.spinner.show();

    const data = {
      id: id
    };

    this.auth.destroy("delete_user", localStorage.getItem('userID'), data).subscribe(
      response => {
        console.log(response);
        this.spinner.hide();
        this.alert.success('User deleted successfully.');
        this.getUsers();
      },
      error => {
        console.log(error);
        this.spinner.hide();
        this.alert.error("Error deleting data, please try again.");
      }
    );
  }

  updateRole(id) {
    // console.log(id);
    this.spinner.show();

    const data = {
      user_id: id,
      user_type: 'ADMIN'
    };

    this.auth.update("make_user_admin", localStorage.getItem('userID'), data).subscribe(
      response => {
        console.log(response);
        this.spinner.hide();
        this.alert.success('User deleted successfully.');
        this.getUsers();
      },
      error => {
        console.log(error);
        this.spinner.hide();
        this.alert.error("Error deleting data, please try again.");
      }
    );
  }
  activate(id) {
    // console.log(id);
    this.spinner.show();

    const data = {
      user_id: id,
      is_activated: 'YES'
    };

    this.auth.update("update_activate", localStorage.getItem('userID'), data).subscribe(
      response => {
        console.log(response);
        this.spinner.hide();
        this.alert.success('User deleted successfully.');
        this.getUsers();
      },
      error => {
        console.log(error);
        this.spinner.hide();
        this.alert.error("Error deleting data, please try again.");
      }
    );
  }
  deactivate(id) {
    // console.log(id);
    this.spinner.show();

    const data = {
      user_id: id,
      is_activated: 'NO'
    };

    this.auth.update("update_activate", localStorage.getItem('userID'), data).subscribe(
      response => {
        console.log(response);
        this.spinner.hide();
        this.alert.success('User deleted successfully.');
        this.getUsers();
      },
      error => {
        console.log(error);
        this.spinner.hide();
        this.alert.error("Error deleting data, please try again.");
      }
    );
  }

  setJData() {
    this.judgeDetails.competition_name = this.makeJudgeForm.controls[
      "competition_name"
    ].value;
  }
  setCData() {
    this.competitiorDetails.competition_name = this.makeCompetitorForm.controls[
      "competition_name"
    ].value;
  }

}
