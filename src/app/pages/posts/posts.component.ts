import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { AlertService } from "../../services/alert.service";
import { NgxSpinnerService } from "ngx-spinner";
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  allPosts: any;
  postlength: any;
  Posts: any;

  isShow = false;

  toggleDisplay() {
    this.isShow = !this.isShow;
  }


  constructor(
    config: NgbModalConfig,
    private modalService: NgbModal,
    private auth: AuthService,
    private alert: AlertService,
    // private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getpost();
  }
  getpost() {
    this.spinner.show();
    this.auth.get('admin_posts').subscribe(
      response => {
        if (response['data']['data'].length > 0 ) {
        console.log(response['data']);
        this.allPosts = response['data']['data'];
        this.postlength = response['data']['data'].length;
        console.log(this.postlength);
        this.spinner.hide();
      } else {
        this.spinner.hide();
          this.alert.info('No Post available yet');
      }
      },

      error => {
        this.spinner.hide();
        this.alert.error('Error loading user Data');
        // console.log(error);
      }
    );
  }
  view(ev) {
    this.auth.show('post', ev).subscribe(
      (response) => {
        console.log(response['data']);
        this.Posts = response['data'];
        console.log(this.Posts);
      },
      (error) => {
        console.log(error);
        this.alert.error('Getting data unsuccessful. Please try again');
      }
    );
  }
  openScrollableContent(singlepost) {
    this.modalService.open(singlepost, { scrollable: true, size: 'lg' });
  }
  delete(id) {
    this.spinner.show();
    const data = {
      id: id
    };

    this.auth.destroy("remove_post", localStorage.getItem('userID'), data).subscribe(
      response => {
        this.spinner.hide();
        this.alert.success(response["message"]);
        this.getpost();
      },
      error => {
        console.log(error);
        this.spinner.hide();
        this.alert.error("Error deleting data, please try again.");
      }
    );
  }
}
